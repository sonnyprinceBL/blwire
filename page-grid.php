<?php
/**
 * Template Name: Grid Page
 *
 * @package blwire
 */

get_header(); ?>


<?php /*

$args = array(
		'post_type'=>'post',
		'post_status'=>'publish',
		'posts_per_page'=> 4
		);

$wpb_all_query = new WP_Query($args); ?>

<?php if ( $wpb_all_query->have_posts() ) : $i = 1 ?>

	<?php while ( $wpb_all_query->have_posts() ) : $wpb_all_query->the_post(); ?>

			<a href="<?php the_permalink(); ?>" class="wire-block wire-mb">

				<?php echo $i ?>
				<?php if ( has_post_thumbnail() ) : ?> 
                	<span class="block news-block block-with-hover with-bg-img" style="background-image: url(<?php the_post_thumbnail_url(); ?>)">
				<?php else : ?>
                	<span class="block news-block block-with-hover">
				<?php endif ; ?>
                    <span class="block-content">
                        <h2 class="block-title <?php if ( has_post_thumbnail() ) : ?>shadowed-text<?php endif ; ?>"><?php the_title(); ?></h2>

                        <p class="block-text-content <?php if ( has_post_thumbnail() ) : ?>shadowed-text<?php endif ; ?>"><?php the_content(); ?></p>
                    </span>
                </span>
            </a>


	<?php $i++; endwhile; ?>

	<?php wp_reset_postdata(); ?>

<?php endif; */?>
<main class="wire-container">
	<?php 

	get_template_part( 'template-parts/content', 'wire-column-1' );
	get_template_part( 'template-parts/content', 'wire-column-2' );
	get_template_part( 'template-parts/content', 'wire-column-3' );

	?>
</main>

<!--         <div class="wire-column">
            <a href="https://insideabbeyroad.withgoogle.com/en" class="wire-block wire-height-third wire-width-full wire-mb">
                <span class="block news-block block-with-hover with-bg-img" style="background-image: url(https://insideabbeyroad.withgoogle.com/static/images/modules/welcome/background.jpg)">
                    <span class="block-content">
                        <h2 class="block-title">Inside Abbey Road</h2>
                        <p class="block-text-content">Not just limited to the App and Google Cardboard – you can take the tour in your web browser ‘of choice’ (read Chrome) too.</p>
                    </span>
                </span>
            </a>
            <div class="row wire-height-third wire-mb">
                <div class="wire-block wire-height-full wire-width-half">
                    <span class="block instagram-block" style="background-image: url(https://scontent-cdg2-1.cdninstagram.com/t51.2885-15/e35/14474052_1687772698212159_596848390826885120_n.jpg);">
                        <span class="instagram-content">By day they call him Rubber Towells. By early evening he goes by the name of The Green King. #brayleinoyucca #creativeagency #digitalagency #agencylife #lawnbowls #BLYeah</span>
                    </span>
                </div>
                <div class="wire-block wire-height-full wire-width-half">
                    <span class="block twitter-block">
                        <span class="block-content">
                            <span class="tweet-meta">
                                <a href="" class="tweet-author">BrayLeinoYucca</a>
                                <span class="tweet-date">30 Sep</span>
                            </span>
                            <p class="tweet-content">The 1 touch payments game changer arrives? Probably. <a href="#">https://t.co/alkdz9cckM</a></p>
                        </span>
                    </span>
                </div>
            </div>
            <div class="wire-block wire-height-third wire-width-full wire-mb">
                <span class="block alert-block">
                    <span class="block-content">
                        <h2 class="block-title alert-text">It’s four o’clock. It’s Friday. The bar is open!</h2>
                    </span>
                </span>
            </div>
        </div>
        <div class="wire-column">
            <a href="http://www.socialmediatoday.com/social-business/facebook-set-launch-facebook-work-next-month" class="wire-block wire-height-half wire-width-full wire-mb">
                <span class="block news-block block-with-hover">
                    <span class="block-content">
                        <h2 class="block-title shadowed-text">Facebook set to launch 'Facebook at Work' next month</h2>
                        <p class="block-text-content shadowed-text">It looks as though Facebook are trying to get into the business internal communications space with FAW.  The article suggests it will be a licence-based, paid for product ($1-$5 per user per month), but I’d imagine of much more interest to Mr Zuckerberg will be the potential for Facebook to link users (and therefore their data) from the FAW platform across to Facebook itself.</p>
                        <p class="mbn block-text-content shadowed-text">Facebook haven’t confirmed that this is what they plan to do but, I’d imagine this is where they see value, as it will enable them to challenge LinkedIn’s ability to target advertising towards people based on their work life - job type, job seniority, industry type, company size etc. and in a more compelling way for advertisers.</p>
                    </span>
                </span>
            </a>
            <a href="#" class="wire-block wire-height-half wire-width-full wire-mb">
                <span class="block news-block block-with-hover with-bg-img" style="background-image: url(https://musiclab.chromeexperiments.com/img/experiments/thumbnail_spectrogram.jpg)">
                    <span class="block-content">
                        <h2 class="block-title">Google Chrome Music Lab</h2>
                        <p class="block-text-content">An interactive monkey playing the kettle drums, real-time sound visualisation and doodles turned into music – what’s not to like?</p>
                    </span>
                </span>
            </a>
        </div>
        <div class="wire-column">
            <a href="#" class="wire-block wire-height-quarter wire-width-full wire-mb">
                <span class="block news-block block-with-hover with-bg-img" style="background-image: url(https://s.aolcdn.com/dims5/amp:6f20e3c7c5204b8878e0b36ce3a921fe3401b354/q:100/?url=http%3A%2F%2Fo.aolcdn.com%2Fdims-shared%2Fdims3%2FGLOB%2Fcrop%2F3600x2400%2B0%2B0%2Fresize%2F1600x1067%21%2Fformat%2Fjpg%2Fquality%2F85%2Fhttp%3A%2F%2Fo.aolcdn.com%2Fhss%2Fstorage%2Fmidas%2F9ef8cef502c455a9140ed1fcd90f2f9f%2F203418917%2Fstock-photo-businessman-and-robot-s-handshake-with-holographic-earth-globe-on-background-artificial-317361941.jpg)">
                    <span class="block-content">
                        <h2 class="block-title">One day you can tell your children that you were there, that day when SKYNET was founded ; )</h2>
                    </span>
                </span>
            </a>
            <div class="wire-block wire-height-half wire-width-full wire-mb">
                <span class="block instagram-block" style="background-image: url(https://scontent-ams3-1.cdninstagram.com/t51.2885-15/e35/14134779_1755281684742395_976254336_n.jpg);">
                    <span class="instagram-content">Summer's not over, it's not over, not over... #agencyshoes #brayleinoyucca #creativeagency #digitalagency #converse #agencylife</span>
                </span>
            </div>
            <div class="wire-block wire-height-quarter wire-width-full wire-mb">
                <span class="block weather-block cloudy">
                    <span class="block-content">
                        <h2 class="weather-block-degrees mbn">16°</h2>
                        <p class="weather-block-text mbn block-text-content">Cloudy with rain throughout the day.</p>
                    </span>
                </span>
            </div>
        </div>
    </main>
 -->

<?php
get_footer();
