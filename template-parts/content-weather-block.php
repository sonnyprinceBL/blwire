<?php
/**
 * Weather block template
 *
 * @package BLWire
 */

?>


<div class="wire-block wire-height-quarter wire-width-full wire-mb">
    <span class="block weather-block cloudy">
        <span class="block-content">
            <h2 class="weather-block-degrees mbn">16°</h2>
            <p class="weather-block-text mbn block-text-content">Cloudy with rain throughout the day.</p>
        </span>
    </span>
</div>