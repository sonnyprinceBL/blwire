<?php
/**
 * Twitter block template
 *
 * @package BLWire
 */

?>

<div class="wire-block wire-height-full wire-width-half">
    <span class="block twitter-block">
        <span class="block-content">
            <span class="tweet-meta">
                <a href="" class="tweet-author">BrayLeinoYucca</a>
                <span class="tweet-date">30 Sep</span>
            </span>
            <p class="tweet-content">The 1 touch payments game changer arrives? Probably. <a href="#">https://t.co/alkdz9cckM</a></p>
        </span>
    </span>
</div>