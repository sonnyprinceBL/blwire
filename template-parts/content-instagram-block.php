<?php
/**
 * Twitter block template
 *
 * @package BLWire
 */

?>

<div class="wire-block wire-height-half wire-width-full wire-mb">
	<span class="block instagram-block" style="background-image: url(https://scontent-ams3-1.cdninstagram.com/t51.2885-15/e35/14134779_1755281684742395_976254336_n.jpg);">
	    <span class="instagram-content">Summer's not over, it's not over, not over... #agencyshoes #brayleinoyucca #creativeagency #digitalagency #converse #agencylife</span>
	</span>
</div>