<?php
/**
 * This is a Wire Columns, Variations 1.
 *
 * @package BLWire
 */

?>

<div class="wire-column">
	
	<?php 

$args = array(
		'post_type'=>'post',
		'post_status'=>'publish',
		'posts_per_page'=> 2
		);

$wpb_all_query = new WP_Query($args); ?>

<?php if ( $wpb_all_query->have_posts() ) : $i = 1 ?>

	<?php while ( $wpb_all_query->have_posts() ) : $wpb_all_query->the_post(); ?>

			<a href="<?php the_permalink(); ?>" class="wire-block wire-height-half wire-width-full wire-mb">

				<?php /* echo $i */ ?>
				<?php if ( has_post_thumbnail() ) : ?> 
                	<span class="block news-block block-with-hover with-bg-img" style="background-image: url(<?php the_post_thumbnail_url(); ?>)">
				<?php else : ?>
                	<span class="block news-block block-with-hover">
				<?php endif ; ?>
                    <span class="block-content">
                        <h2 class="block-title <?php if ( !has_post_thumbnail() ) : ?>shadowed-text<?php endif ; ?>"><?php the_title(); ?></h2>

                        <div class="block-text-content <?php if ( !has_post_thumbnail() ) : ?>shadowed-text<?php endif ; ?>"><?php the_content(); ?></div>
                    </span>
                </span>
            </a>


	<?php $i++; endwhile; ?>

	<?php wp_reset_postdata(); ?>

<?php endif ; ?>

</div>